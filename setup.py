#!/usr/bin/enc python3
""" Short description """

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="human_resources_module_by_tirpitz",  # Replace with your own username
    version="0.0.2",
    author="Mikołaj Błażejewski",
    author_email="",
    description="Module for human resources applications",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Tirpitz_Player/akademia-python-2020-human-resources-module",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)