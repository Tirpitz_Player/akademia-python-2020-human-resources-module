#!/usr/bin/enc python3
""" Short description """

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.2"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""


class Human:
    def __init__(self, name: str, surname: str):
        """
        Human Class
        :param name: First name
        :param surname: Last name
        """
        self._name = name  # If is "_" after "." then this variable is protected
        self._surname = surname  # If is "_" after "." then this variable is protected

    @property  # Thanks to this, the class will behave like a variable
    def name(self):
        return self._name

    @property  # Thanks to this, the class will behave like a variable
    def surname(self):
        return self._surname

