#!/usr/bin/enc python3
""" Short description """

# Import from other files here
from tests.human_resources_module_tests.human_tests import HumanTest

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.2"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Import of libraries
import unittest

if __name__ == "__main__":
    unittest.main()
