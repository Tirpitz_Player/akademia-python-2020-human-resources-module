#!/usr/bin/enc python3
""" Short description """

# Import from other files here
from human_resources_module.human import Human

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Import of libraries
import unittest


class HumanTest(unittest.TestCase):
    def test_jan_kowalski(self):
        jan = Human("Jan", "Kowalski")
        self.assertEqual("Jan", jan.name, "There is name other than expected")
        self.assertEqual("Kowalski", jan.surname, "There is surname other than expected")

    def test_anna_nowak(self):
        jan = Human("Anna", "Nowak")
        self.assertEqual("Anna", jan.name, "There is name other than expected")
        self.assertEqual("Nowak", jan.surname, "There is surname other than expected")


if __name__ == "__main__":
    unittest.main()
